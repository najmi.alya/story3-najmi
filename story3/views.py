from django.shortcuts import render

def homepage(request):
    return render(request, 'homepage.html', {})

def experience(request):
    return render(request, 'experience.html', {})

def story(request):
    return render(request, 'story.html', {})

def story1(request):
    return render(request, 'story1.html', {})