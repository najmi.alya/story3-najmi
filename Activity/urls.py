from django.urls import path
from . import views

app_name="activities"
urlpatterns = [
    path('', views.index, name='index'),
    path('newActivity/', views.newActivity, name="newActivity"),
    path('listActivities/', views.listActivities, name="listActivities"),
    # path('listActivities/<int:activities_id>', views.addParticipant, name="addParticipant"),
    path('addParticipant/', views.addParticipant, name="addParticipant"),
    # path('delete/', views.deleteActivity, name='delete')
]