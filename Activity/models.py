from django.db import models

# Create your models here.
class Activities(models.Model):
    Activity_Name = models.CharField(max_length=50)

    def __str__(self):
        return self.Activity_Name

class Participant(models.Model):
    Name = models.CharField(max_length=50)
    activities = models.ForeignKey(Activities, on_delete=models.CASCADE, default='')

    def __str__(self):
        return "{}.{}".format(self.activities, self.Name)