from django.shortcuts import render, redirect
from .models import Activities, Participant
from .forms import formActivities, formParticipant

# Create your views here.
def index(request):
    kegiatan = Activities.objects.all()
    context = {"kegiatan":kegiatan}
    return render(request, 'Activity.html', context)

def newActivity(request):
    form = formActivities()
    if request.method == "POST":
        form = formActivities(request.POST)
        if form.is_valid():
            form.save()
            return redirect("activities:listActivities")
    
    context = {
        'kegiatan':form,
    }
    return render(request, "newActivity.html", context)

def listActivities(request):
    all_form = Activities.objects.all()
    person = Participant.objects.all()
    kegiatan = []
    person_form = formParticipant()
    for namaKegiatan in all_form:
        personAct = []
        for prsn in person:
            if prsn.activities_id == namaKegiatan.id:
                personAct.append(prsn)
        kegiatan.append((namaKegiatan, personAct))

    context = {
        'kegiatan':kegiatan,
        'person':person,
        'person_form':person_form
    }
    return render(request, 'allActivities.html', context)

def addParticipant(request):
    # nama = request.POST.get('nama', None)
    # Participant.objects.create(Name=nama, activities=Activities.objects.get(id=activities_id))
    form = formParticipant(request.POST)
    form.save()
    return redirect('activities:listActivities')

# def deleteActivity(request, pk):
#     Activities.objects.filter(id=pk).delete()
#     return redirect('activities:listActivities')
