from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *
from .models import Activities, Participant
from .forms import formActivities, formParticipant

# Create your tests here.
class ActivitiesUnitTest(TestCase):
    def test_Activity_url_is_exist(self):
        response = Client().get('/Activity/')
        self.assertEqual(response.status_code, 200)
    
    def test_listActivities_url_is_exist(self):
        response = Client().get('/Activity/listActivities')
        self.assertEqual(response.status_code, 301)
    
    def test_addParticipant_url_is_exist(self):
        response = Client().get('/Activity/addParticipant')
        self.assertEqual(response.status_code, 301)
    
    def test_newActivity_url_is_exist(self):
        response = Client().get('/Activity/newActivity')
        self.assertEqual(response.status_code, 301)
    
    def test_url_using_func(self):
        found = resolve('/Activity/')
        self.assertEqual(found.func, index)

    def test_activities(self):
        newActivity = Activities.objects.create(Activity_Name='kegiatan')
        countActivity = Activities.objects.all().count()
        self.assertEqual(countActivity, 1)
    
    def test_create_participant(self):
        kegiatan = Activities.objects.create(Activity_Name='activity')
        Participant.objects.create(Name='participant', activities=kegiatan)
        countParticipant = Participant.objects.all().count()
        self.assertEqual(countParticipant, 1)

    def test_form_is_blank(self):
        form = formActivities(data= {'Activity_Name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['Activity_Name'],
            ["This field is required."]
            )
    
    def test_template_used(self):
        response = Client().get('/Activity/')
        self.assertTemplateUsed(response, 'Activity.html')
    
    def test_template_used_list(self):
        newActivity = Activities.objects.create(Activity_Name='kegiatan')
        response = Client().get('/Activity/listActivities/')
        self.assertTemplateUsed(response, 'allActivities.html')
    
    def test_post_form(self):
        response = Client().post('/Activity/newActivity/',{
            'Activity_Name':''
        })
        self.assertIn('', response.content.decode())
    
    def test_get_Activity(self):
        Activities.objects.create(Activity_Name='kegiatan')
        activity = Activities.objects.get(Activity_Name='kegiatan')
        self.assertEqual(str(activity), 'kegiatan')
    
    def test_views_listActivities(self):
        found = resolve('/Activity/listActivities/')
        self.assertEqual(found.func, listActivities)
    
    def test_views_addParticipant(self):
        found = resolve('/Activity/addParticipant/')
        self.assertEqual(found.func, addParticipant)
    
    def test_views_newActivity(self):
        found = resolve('/Activity/newActivity/')
        self.assertEqual(found.func, newActivity)
    
    def test_template_used_newActivity(self):
        response = Client().get('/Activity/newActivity/')
        self.assertTemplateUsed(response, 'newActivity.html')
    
