from django import forms
from .models import Activities, Participant

class formActivities(forms.ModelForm):
    class Meta:
        model = Activities
        fields = ('Activity_Name',)

        widgets = {
            'Activity_Name' : forms.TextInput(attrs={'class': 'forms-control'}),
        }

class formParticipant(forms.ModelForm):
    class Meta:
        model = Participant
        fields = ('Name', 'activities')

        widgets = {
            'Name' : forms.TextInput(attrs={'class': 'forms-control'}),
        }