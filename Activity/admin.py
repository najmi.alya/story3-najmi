from django.contrib import admin

# Register your models here.
from .models import Activities, Participant

admin.site.register(Activities)
admin.site.register(Participant)