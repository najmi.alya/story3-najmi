from django.db import models

# Create your models here.
class Subject(models.Model):
    SMS = (
        ('2019/2020', '2019/2020'),
        ('2020/2021', '2020/2021'),
    )
    Nama_mata_kuliah = models.CharField(max_length=100)
    Nama_dosen = models.CharField(max_length=100)
    Nama_kelas = models.CharField(max_length=100)
    Jumlah_sks = models.CharField(max_length=100)
    Semester = models.CharField(max_length=100, choices=SMS)
    Deskripsi_mata_kuliah = models.CharField(max_length=100)

def __str__(self):
    return self.Nama_mata_kuliah