from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import subjectForm
from .models import Subject

# Create your views here.
def index(request):
    return render(request, 'form.html')

def add(request):
    form_class = subjectForm()
    if request.method == "POST":
        #print('Printing POST:', request.POST)
        form_class = subjectForm(request.POST)
        if form_class.is_valid():
            form_class.save()
            return redirect('/Subject')

    context = {'form':form_class}
    return render(request, 'add.html', context)

def matkul(request, pk):
    form = Subject.objects.get(id = pk)
    form_hasil = Subject.objects.filter(id=pk)
    context = {'form_hasil':form_hasil}
    return render(request, 'all.html', context)

def all(request):
    form_hasil = Subject.objects.all()
    context = {'form_hasil':form_hasil}
    return render(request, 'matkul.html', context)

def semuanya(request):
    form_hasil = Subject.objects.all()
    context = {'form_hasil':form_hasil}
    return render(request, 'all.html', context)

def updateSubject(request, pk):
    form = Subject.objects.get(id = pk)
    form_class = subjectForm(instance=form)
    if request.method == "POST":
        form_class = subjectForm(request.POST, instance=form)
        if form_class.is_valid():
            form_class.save()
            return redirect('/Subject/all')

    context = {'form':form_class}
    return render(request, 'add.html', context)

def deleteSubject(request, pk):
    Subject.objects.filter(id=pk).delete()
    return redirect('/Subject/all')