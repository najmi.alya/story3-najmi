from django import forms
from .models import Subject

class subjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = ('Nama_mata_kuliah', 'Nama_dosen', 'Nama_kelas', 'Jumlah_sks', 'Semester', 'Deskripsi_mata_kuliah')

        widgets = {
            'Nama_mata_kuliah' : forms.TextInput(attrs={'class': 'forms-control'}),
            'Nama_dosen' : forms.TextInput(attrs={'class': 'forms-control'}),
            'Nama_kelas' : forms.TextInput(attrs={'class': 'forms-control'}),
            'Jumlah_sks' : forms.TextInput(attrs={'class': 'forms-control'}),
            'Semester' : forms.TextInput(attrs={'class': 'forms-control'}),
            'Deskripsi_mata_kuliah' : forms.TextInput(attrs={'class': 'forms-control'}),
        }

