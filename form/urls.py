from django.urls import path
from . import views

app_name = "form"
urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='tambah'),
    path('all/', views.all, name='semua'),
    path('semua/', views.semuanya, name='semuanya'),
    path('matkul/<str:pk>', views.matkul, name='matkul'),
    path('update/<str:pk>', views.updateSubject, name='update'),
    path('delete/<str:pk>', views.deleteSubject, name='delete')
]